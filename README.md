# Bienvenido a desy.aragon.es 👋

> desy.aragon.es es el sitio web de Desy, sistema de diseño del Gobierno de Aragón, que utiliza el paquete de npm desy-html como dependencia.

### 🏠 [Sitio web](https://desy.aragon.es/)

## Prerrequisitos

- npm >=6.14.0
- node >=12.18.0

## Instalación

```sh
npm install
```

## Primeros pasos

Para compilar el proyecto para producción en la carpeta /dist: compila el HTML, CSS purgeado y minificado para producción y Javascript:

```sh
npm run prod
```

Para desarrollar el proyecto: compila el HTML, CSS con todas las clases de Tailwind CSS y Javascript y escucha cambios en los archivos con Browser sync. Para ver el resultado abre una ventana del navegador e introduce la dirección http://localhost:3000/

```sh
npm run dev
```

## Autor

SDA Servicios Digitales de Aragón


## Licencia

This project is [EUPL--1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) licensed.
